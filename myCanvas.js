var canvas = document.getElementById('mycanvas');
var ctx = canvas.getContext('2d');
var textbox = document.getElementById('textBox');
var x = 0;
var y = 0;
var mode;
var isdrawing = false;
var last_x = 0;
var last_y = 0;
var start_x = 0;
var start_y = 0;
var textflag = false;
var textcontent = "";
var undo_stack = [];
var redo_stack = [];
//var rainbow = 0;
ctx.lineCap = 'round';

function getMousePos(canvas, evt){
    var rect = canvas.getBoundingClientRect(); //取得物件完整的座標資訊
    return {
        x: evt.clientX - rect.left,
        y: evt.clientY - rect.top
    }; //回傳滑鼠在canvas上的座標
}

canvas.addEventListener('mouseup', function(){
    isdrawing = false;
}, false); 

canvas.addEventListener('mouseout', function(){
    isdrawing = false;
}, false); 

canvas.addEventListener('mousemove', Draw);
function Draw(evt) {
    if(!isdrawing) return;

    if(mode == "draw"){
        ctx.globalCompositeOperation = 'source-over';
        ctx.setLineDash([0]);
        ctx.beginPath();
        ctx.moveTo(last_x,last_y);
        ctx.lineTo(evt.offsetX, evt.offsetY); //畫線到結束位置
        ctx.stroke(); //draw
        [last_x, last_y] = [evt.offsetX, evt.offsetY];
    }else if(mode == "erase"){
        ctx.globalCompositeOperation = 'destination-out';
        ctx.setLineDash([0]);
        ctx.beginPath();
        ctx.moveTo(last_x,last_y);
        ctx.lineTo(evt.offsetX, evt.offsetY); //畫線到結束位置
        ctx.stroke(); //draw
        [last_x, last_y] = [evt.offsetX, evt.offsetY];
    }else if(mode == "rect"){
        ctx.globalCompositeOperation = 'source-over';
        ctx.setLineDash([0]);
        var tmp;
        ctx.putImageData(tmp=undo_stack.pop(), 0, 0);
        undo_stack.push(tmp);
        ctx.beginPath();
        ctx.moveTo(start_x, start_y);
        ctx.fillRect(start_x, start_y, evt.offsetX-start_x, evt.offsetY-start_y);
        ctx.stroke(); //draw
        [last_x, last_y] = [evt.offsetX, evt.offsetY];
    }else if(mode == "circle"){
        ctx.globalCompositeOperation = 'source-over';
        ctx.setLineDash([0]);
        var tmp;
        ctx.putImageData(tmp=undo_stack.pop(), 0, 0);
        undo_stack.push(tmp);
        ctx.beginPath();
        var radious = Math.sqrt((start_x-last_x)*(start_x-last_x) + (start_y-last_y)*(start_y-last_y));
        //ctx.moveTo(start_x, start_y);
        ctx.arc(start_x, start_y, radious, 0, Math.PI*2);
        ctx.fill();
        ctx.stroke();
        [last_x, last_y] = [evt.offsetX, evt.offsetY];
    }else if(mode == "triangle"){
        ctx.globalCompositeOperation = 'source-over';
        ctx.setLineDash([0]);
        var tmp;
        ctx.putImageData(tmp=undo_stack.pop(), 0, 0);
        undo_stack.push(tmp);
        ctx.beginPath();
        var edge = evt.offsetX - start_x;
        ctx.moveTo(start_x, start_y);
        ctx.lineTo(evt.offsetX, evt.offsetY);
        ctx.lineTo(start_x-edge, evt.offsetY);
        ctx.closePath();
        ctx.fill();
        ctx.stroke();
    }else if(mode == "erect"){
        ctx.globalCompositeOperation = 'source-over';
        ctx.setLineDash([0]);
        var tmp;
        ctx.putImageData(tmp=undo_stack.pop(), 0, 0);
        undo_stack.push(tmp);
        ctx.beginPath();
        ctx.moveTo(start_x, start_y);
        ctx.strokeRect(start_x, start_y, evt.offsetX-start_x, evt.offsetY-start_y);
        ctx.stroke(); //draw
        [last_x, last_y] = [evt.offsetX, evt.offsetY];
    }else if(mode == "ecircle"){
        ctx.globalCompositeOperation = 'source-over';
        ctx.setLineDash([0]);
        var tmp;
        ctx.putImageData(tmp=undo_stack.pop(), 0, 0);
        undo_stack.push(tmp);
        ctx.beginPath();
        var radious = Math.sqrt((start_x-last_x)*(start_x-last_x) + (start_y-last_y)*(start_y-last_y));
        //ctx.moveTo(start_x, start_y);
        ctx.arc(start_x, start_y, radious, 0, Math.PI*2);
        //ctx.fill();
        ctx.stroke();
        [last_x, last_y] = [evt.offsetX, evt.offsetY];
    }else if(mode == "etriangle"){
        ctx.globalCompositeOperation = 'source-over';
        ctx.setLineDash([0]);
        var tmp;
        ctx.putImageData(tmp=undo_stack.pop(), 0, 0);
        undo_stack.push(tmp);
        ctx.beginPath();
        var edge = evt.offsetX - start_x;
        ctx.moveTo(start_x, start_y);
        ctx.lineTo(evt.offsetX, evt.offsetY);
        ctx.lineTo(start_x-edge, evt.offsetY);
        ctx.closePath();
        //ctx.fill();
        ctx.stroke();
    }else if(mode == "line"){
        ctx.globalCompositeOperation = 'source-over';
        ctx.setLineDash([0]);
        var tmp;
        ctx.putImageData(tmp=undo_stack.pop(), 0, 0);
        undo_stack.push(tmp);
        ctx.beginPath();
        ctx.moveTo(start_x,start_y);
        ctx.lineTo(evt.offsetX, evt.offsetY); //畫線到結束位置
        ctx.stroke(); //draw
        [last_x, last_y] = [evt.offsetX, evt.offsetY];
    }else if(mode == "dline"){
        ctx.globalCompositeOperation = 'source-over';
        var tmp;
        ctx.putImageData(tmp=undo_stack.pop(), 0, 0);
        undo_stack.push(tmp);
        ctx.setLineDash([5,15]);
        ctx.beginPath();
        ctx.moveTo(start_x,start_y);
        ctx.lineTo(evt.offsetX, evt.offsetY); //畫線到結束位置
        ctx.stroke(); //draw
        [last_x, last_y] = [evt.offsetX, evt.offsetY];
    }
    
};

canvas.addEventListener('mousedown', function(evt){
    undo_stack.push(ctx.getImageData(0,0,canvas.width, canvas.height));
    redo_stack = [];
    [last_x, last_y] = [evt.offsetX, evt.offsetY];
    isdrawing = true;
    if(mode == "rect" || "circle" || "triangle" || "erect" || "ecircle" || "etriangle" || "line" || "dline"){
        [start_x, start_y] = [last_x, last_y]; 
    }
    if(mode == "text"){
        var x1 = evt.offsetX;
        var y1 = evt.offsetY;
        if(textflag){ //寫字
            textcontent = textbox.value;
            textflag = false;
            textbox.style['z-index'] = -1;
            textbox.value = "";
            Writing();
        }else{//輸入
            textflag = true;
            textbox.style.left = x1 + 'px';
            textbox.style.top = y1 + 'px';
            textbox.style['z-index'] = 6;
        }
    }
});

function Writing(){
    var font = document.getElementById('font');
    var index1 = font.selectedIndex;
    var fontsize = document.getElementById('fontsize');
    var index2 = fontsize.selectedIndex;
    ctx.save();
    ctx.beginPath();
    ctx.font =  fontsize.options[index2].value + "px " + font.options[index1].value;
    //ctx.font = "20px serif";
    ctx.fillText(textcontent, parseInt(textbox.style.left), parseInt(textbox.style.top))
    ctx.restore();
    ctx.closePath();
}

function ChangeSize(){
    ctx.lineWidth = document.getElementById('pencilsize').value;
}

function ChangeColor(){
    ctx.strokeStyle = document.getElementById('color').value;
    ctx.fillStyle = document.getElementById('color').value;
}

function Pencil(){
    //document.getElementById("screen").value = "1";
    document.body.style.cursor = "url('img/pencil2.png'), auto";
    mode = "draw";
}

function Eraser(){
    //document.getElementById("screen").value = "2";
    document.body.style.cursor = "url('img/eraser2.png'), auto";
    mode = "erase";
}

function Addtext(){
    document.body.style.cursor = "url('img/textbox2.png'), auto";
    mode = "text";
}

function Rect(){
    document.body.style.cursor = "url('img/rect2.png'), auto";
    mode = "rect";
}

function Circle(){
    document.body.style.cursor = "url('img/circle2.png'), auto";
    mode = "circle";
}

function Triangle(){
    document.body.style.cursor = "url('img/triangle2.png'), auto";
    mode = "triangle";
}

function eRect(){
    document.body.style.cursor = "url('img/erect2.png'), auto";
    mode = "erect";
}

function eCircle(){
    document.body.style.cursor = "url('img/ecircle2.png'), auto";
    mode = "ecircle";
}

function eTriangle(){
    document.body.style.cursor = "url('img/etriangle2.png'), auto";
    mode = "etriangle";
}

function Line(){
    document.body.style.cursor = "url('img/line2.png'), auto";
    mode = "line";
}

function dLine(){
    document.body.style.cursor = "url('img/dline2.png'), auto";
    mode = "dline";
}

function Reset(){
    document.body.style.cursor = 'default';
    ctx.clearRect(0, 0, canvas.width, canvas.height);
}

function Undo(){
    if(undo_stack.length > 0){
        redo_stack.push( ctx.getImageData(0, 0, canvas.width, canvas.height) );
        ctx.putImageData(undo_stack.pop(), 0, 0);
    }
}

function Redo(){
    if(redo_stack.length > 0){
        undo_stack.push( ctx.getImageData(0, 0, canvas.width, canvas.height) );
        ctx.putImageData(redo_stack.pop(), 0, 0);
    }
}

function Download(){
    var image = canvas.toDataURL("image/png").replace("image/png", "image/octet-stream");
    document.getElementById("download").setAttribute("href", image);
}

function Upload(e){
    var reader = new FileReader(); //用來讀取file資料的FileReader
    reader.onload = function(event){
        var img = new Image();
        img.onload = function(){
            ctx.drawImage(img, 0, 0);
        }
        img.src = event.target.result;
    }
    reader.readAsDataURL(e.target.files[0]); //可將讀取到的file資料轉換成data的url
}

/*function Rainbow(){
    document.body.style.cursor = "url('img/brush.png'), auto";
    ctx.strokeStyle = `hsl(${rainbow}, 100%, 50%)`;
    rainbow++;
    if(rainbow >= 360){
        rainbow = 0;
    }
}*/
//rainbow的部分未完成